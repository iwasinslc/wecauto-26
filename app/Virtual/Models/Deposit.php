<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Deposit",
 *     description="Deposit information"
 * )
 */
class Deposit
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Deposit ID",
     *     example="b5d00f20-ff1d-11ea-8e8c-211d59c5f58c"
     * )
     *
     * @var string
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Value",
     *     description="Invested amount",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $value;

    /**
     * @OA\Property(
     *     title="Currency",
     *     description="Currency code of the invested amount",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $currency;

    /**
     * @OA\Property(
     *     title="Duration",
     *     description="Number of duration days for deposit",
     *     example=365
     * )
     *
     * @var integer
     */
    public $duration;

    /**
     * @OA\Property(
     *     title="Status",
     *     description="Status of the deposit",
     *     example="active"
     * )
     *
     * @var string
     */
    public $status;

    /**
     * @OA\Property(
     *     title="Fast info",
     *     description="Information about increasing",
     *     type="object",
     *     @OA\Property(
     *          property="value",
     *          title="Value",
     *          description="Amount of money spent for fast deposit",
     *          type="number",
     *          format="float"
     *    ),
     *    @OA\Property(
     *          property="currency",
     *          title="Currency",
     *          description="Currency of fst value",
     *          example="USD",
     *          type="string"
     *    ),
     *    @OA\Property(
     *          property="days",
     *          title="Days",
     *          description="Number of fasted days",
     *          example=10,
     *          type="integer"
     *    ),
     * )
     *
     * @var string
     */
    public $fst;

    /**
     * @OA\Property(
     *     title="Closing date",
     *     description="Date of the deposit closing",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $closing_at;

    /**
     * @OA\Property(
     *     title="Created At",
     *     description="Date of the creating deposit",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $created_at;
}