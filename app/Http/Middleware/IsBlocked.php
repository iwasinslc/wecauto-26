<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;

class IsBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (user()->is_blocked) {
            return response()->view('customer.is_blocked');
        }

        return $next($request);
    }
}
