<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LicenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'levels' => $this->levels,
            'name' => $this->name,
            'deposit_min' => $this->deposit_min,
            'deposit_max' => $this->deposit_max,
            'moto_min' => $this->moto_min,
            'currency' => getCurrencyById($this->currency_id)->code,
            'duration' => $this->duration,
            'sell_limit' => $this->sell_amount,
            'buy_limit' => $this->buy_amount
        ];
    }
}
