<?php
namespace App\Http\Controllers\Telegram\account_bot\Topup;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\PaymentSystem;
use App\Models\Rate;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class RateController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        preg_match('/topup\_rate '.Constants::UUID_REGEX.'/', $event->text, $data);

        if (isset($data[1]))
        {
            $currency_id = $data[1];
        }
        else {
            return response('ok');
        }


        $user = $telegramUser->user;

        $topup_data =  cache()->get('topup_data'.$user->id);

        if ($topup_data==null)
        {
            $error = __('Time to invest has expired');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }


        $topup_data['currency_id'] = $currency_id;

        cache()->put('topup_data'.$user->id, $topup_data , 30 );



        $message = view('telegram.account_bot.topup.rate', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        $rates = Rate::where('active', 1)
            ->where('currency_id', $currency_id)
            ->orderBy('min')
            ->get();

        $keyboard = [];

        foreach ($rates as $rate) {
            $name = preg_replace('/\[.+\]/', '', $rate->name);

            $keyboard[] = [
                ['text' => trim($name).', '.__('min:').' '.$rate->min.', '.__('max:').' '.$rate->max, 'callback_data' => 'topup_amount '.$rate->id],
            ];

        }

        \Log::info(print_r($keyboard,true));

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                ],
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function validationFailed(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        string $error
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.topup.error', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'error' => $error
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }
    }
}