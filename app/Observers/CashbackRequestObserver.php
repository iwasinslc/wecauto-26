<?php

namespace App\Observers;

use App\Models\CashbackRequest;

class CashbackRequestObserver
{
    /**
     * @param CashbackRequest $cashbackRequest
     * @return array
     * @throws \Exception
     */
    private function getCacheKeys(CashbackRequest $cashbackRequest): array
    {
        return [
            'a.cashbackRequestsCount',
            'i.isCashbackRequestSent.' . $cashbackRequest->user_id
        ];
    }

    /**
     * @param CashbackRequest $cashbackRequest
     * @return array
     */
    private function getCacheTags(CashbackRequest $cashbackRequest): array
    {
        return [];
    }

    /**
     * Listen to the CashbackRequest created event.
     *
     * @param CashbackRequest $cashbackRequest
     * @return void
     * @throws
     */
    public function created(CashbackRequest $cashbackRequest)
    {
        clearCacheByArray($this->getCacheKeys($cashbackRequest));
        clearCacheByTags($this->getCacheTags($cashbackRequest));
    }

    /**
     * Listen to the CashbackRequest deleting event.
     *
     * @param CashbackRequest $cashbackRequest
     * @return void
     * @throws
     */
    public function deleted(CashbackRequest $cashbackRequest)
    {
        clearCacheByArray($this->getCacheKeys($cashbackRequest));
        clearCacheByTags($this->getCacheTags($cashbackRequest));
    }

    /**
     * Listen to the CashbackRequest updating event.
     *
     * @param CashbackRequest $cashbackRequest
     * @return void
     * @throws
     */
    public function updated(CashbackRequest $cashbackRequest)
    {
        clearCacheByArray($this->getCacheKeys($cashbackRequest));
        clearCacheByTags($this->getCacheTags($cashbackRequest));
    }
}
