<?php

namespace App\Events;

use App\Models\Wallet;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MyBalanceUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $broadcastQueue = 'default';

    /** @var array $balanceData */
    public $balanceData;

    /**
     * MyBalanceUpdated constructor.
     * @param Wallet $wallet
     */
    public function __construct(Wallet $wallet)
    {
        /** @var array balanceData */
        $this->balanceData = [
            'id' => $wallet->id,
            'owner_id' => $wallet->user_id,
            'balance' => number_format($wallet->balance, 2),
        ];
    }

    /**
     * @return array|Channel|Channel[]
     */
//    public function broadcastOn()
//    {
//        $ownerId = $this->balanceData['owner_id'];
//        return ['App.User.'.md5($ownerId.env('APP_KEY'))];
//    }


    public function broadcastOn()
    {
        $ownerId = $this->balanceData['owner_id'];
        return new PrivateChannel('balance.user.'.$ownerId);
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'my-balance-updated';
    }
}
