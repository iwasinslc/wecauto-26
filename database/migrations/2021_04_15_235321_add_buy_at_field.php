<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBuyAtField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'buy_at')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dateTime('buy_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'buy_at')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('buy_at');
            });
        }
    }
}
